#!/bin/sh -e
# Converts html into text using the lynx cli browser

HTMLDIR=${HTMLDIR:-"out"}
OUTDIR=${OUTDIR:-"txt"}

echo "Using html source at $HTMLDIR"
echo "Installing to $OUTDIR"

if ! [ -d "$HTMLDIR" ]; then
	echo "No html source found!"
	echo "Please set the HTMLDIR variable correctly, or generate the source with ./make.sh"
fi

if [ -d "$OUTDIR" ]; then
	rm -r "$OUTDIR"
fi

mkdir -p "$OUTDIR"

for f in $(find "$HTMLDIR" -type f | grep .html); do
	echo "Processing $f"
	f2="$(basename "$(echo "$f" | sed "s/.html/.txt/g")")"
	lynx -dump "$f" > "$OUTDIR"/"$f2"
done
