# Kiss Linux

TODO: stop documenting useless distros

Kiss linux is a minimalistic linux distribution which attempts to create a
simple UNIX environment, built opon its own simple package manager written in
POSIX shell, which respects user freedom. Its main feature is its inmensely
simple yet powerful package manager, which allows near-complete and easy user
control of the system. Because of this the project is now completely community
driven after its creator, Dylan Araps, turned into a farmer, which is something
that all free software projects should aim to do. It uses the musl libc instead
of the more common bloated glibc, and uses wayland as its display server by
default.

The package manager, (which is the core of their system) basically uses
traditional "./configure && make" build scripts to build each package, so it
is extremely hackable and has a very simple repo structure.

Kiss addresses a lot of problems in mainstream linux like dbus, DE madness and
glibc, which is of course pretty good. It is an incredibly useful tool for
os-agnostic package management as well.

The main emphasis of kiss is to create a completely hackable linux distro
which is simple in design and easy to maintain. This is of course admirable.

See also:
[oasis](oasis.md)
