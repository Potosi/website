# oasis

Oasis is a suckless linux distribution, which appears on the rocks page on
suckless.org. It is basically a build system composed of lua scripts which
generate ninja build files, which then contain the instructions to compile the
software. This is incredibly efficient as it minimizes build times, but also
ensures complete reproducibility.

The reason why this is done is because another goal of this project is to build
all programs [statically](static-vs-dynamic-linking.md), and many packages
simply will pose problems with their shitty autoconf scripts and build systems.
That also removes dependencies like cmake and meson.

Oasis's root directory is managed in an interesting non-standard way, simliar
to stali's. The oasis build system will commit to the root directory using git
then all new builds are treated as git commits, meaning that you can also
revert back to previous builds if you encounter an error. It also enables the
possibility of pulling the complete root directory from an external git repo,
avoiding the build process completely. I am working on hosting my own oasis
sysroots and sets for you to pull from :)

The root directory is configured by "config.lua", a lua script containing all
the sets and packages you wish to install, including the cflags and other
relevant variables. You then run "./setup.lua" to generate the ninja build
files.

Interestingly oasis in itself can be used as an extension of any linux
distribution, simply by adding the path to "config.prefix" in config.lua and
installing the root dir to /usr/local, for example.

It is important to note that the "/etc" dir is not managed by oasis, but
rather can be another git repo, for which templates are provided.

Oasis uses very different software compared to modern linux distributions. It
uses vis instead of vim, velox instead of xorg, bearssl instead of libressl or
openssl, musl instead of glibc, oksh instead of bash, perp and sinit instead of
systemd or runit...

A good tool to use alongside oasis is the [kiss](kiss-linux.md) package manager
for the packages which aren't provided by it. Also, installing packages through
nix, pkgsrc and flatpak is completely supported. By doing this, it is
completely possible to use oasis as your main desktop OS. It is also ideal for
minimal, restricted environments (as it is beautifully small) and for servers.

Oasis is in theory the perfect linux environment, however there are a great
ammount of barriers that exist that basically impede me to use it, like my
dependency on complex software like jack/pipewire and DAWs to record music,
etc, which rely on specific daemons like elogind, dbus, and policykit. There
are also some weird issues with the window manager: shrinking st too much will
cause velox to crash, subsurfaces are not supported meaning that librewolf
won't work.

## sauna

This has been a thought in my mind for a pretty long time, which is basically
to reuse the oasis build system and build a cross-platform repository for
building static binaries (only across UNIX systems, of course).
Basically, changes will include:
* No git, or any VCS
* Support for OpenBSD
* moar (I actually mean less by that)
I am probably too lazy to do anything resembling work right now, and there is
not any urgency for this whatsoever. I sometimes doubt if it really is worth
it, probably not xD.
