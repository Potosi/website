# Nix - a cool way to build fucked up software

See [package-manager](package-manager.md)

TLDR; kill whoever made nix.

Nix is a fucking stupid package manager/programming language/operating system
which is written in C++ which provides nothing new that couldn't have been
accomplished in the early 90s with a simple SHELL script and a chroot
environment.

1. Nix claims "reproducible builds," however I don't need my programs to
reproduce, and probably almost nobody does.
2. Nix claims "setup fast build environments," however chroots exist (and in
cross compilation Nix is just a fucking wrapper for a chroot!)
3. Nix claims "powerful language," however I don't need anything other than
POSIX shell or fucking rc to do something that's better.
4. Nix claims "setup build environments declaratively," yet the fucking
configurations change every month and dirty workarounds have to be made to
keep the old configurations working. Bruh why do I even need this.
5. Nix offers no customizability of installed packages (useflags, etc)
6. Nix is a security nightnmare, it took 6 days for Hydra (the fucking server
that compiles all of Nix's binaries) to revert back to safe versions of xz
and lzma.
7. Nix uses glibc which is fucking stupid, specially given that a lightweigt
libc like musl is absolutely the best choice for this kind of setup.
8. Nix doesn't follow FHS while theoratically it could and it **should**, but
no, they absolutely *know what they are doing*. I guess they could just symlink
to FHS from /nix for the most part.
9. Nix makes excessive use of hashing, which results in unreadable /nix dir,
which is unfixable without the help of another program and hurts the user's
control over it. Basically a stupid structure.
10. Nixpkgs is the most fucking retarded software repository in history
11. Nix uses systemd, and offers no abstraction layer between init systems

To replace nix, you can do the following:
* Bootstrap a chroot with tools like [kiss](kiss.md) or [oasis](oasis.md), or
download one from alpine.
* Use a normal package manager like a normal person
* OR, make yourself a script to manage packages declaratively

Anything that can be simplified using POSIX SHELL should be writen in POSIX
SHELL.
